﻿Shader "eqsan/GlassShader" {
	Properties {
	}
	SubShader {
		Tags {
			"RenderType" = "Transparent"
			"Queue" = "Transparent"
		}
		LOD 200

		GrabPass {"_BackgroundTexture"}

		// Draw stencil by front surface, to indicate area that's inhibited to draw for back surfaces.
		Pass {
			Stencil {
				Ref 255
				WriteMask 8
				Pass keep
				Fail keep
				ZFail replace
			}
			Cull Back
			ZWrite Off

			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#include "UnityCG.cginc"

			float4 vert(float3 v_pos: POSITION) : SV_POSITION {
				return UnityObjectToClipPos(v_pos);
			}
			float4 frag(float4 pos: SV_POSITION) : SV_TARGET {
				return float4(0, 0, 0, 0);
			}
			ENDCG
		}

		// Only draw when allowed by front. Depth is ignored.
		Pass {
			Stencil {
				Ref 8
				Comp NotEqual
			}
			ZTest Always
			Cull Front

			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0

			#include "UnityCG.cginc"
			#include "Assets/individual/eqsan_sculpture/rays.cginc"

			#define MAX_NUM_STEP 256
			#define EPS_DIST 1e-5

			struct v2f {
				float4 vertex : SV_POSITION;
				float3 posModel : TEXCOORD1;
				float4 grabPos : TEXCOORD2;
				float4 screenPos : TEXCOORD3;
			};

			sampler2D _BackgroundTexture;

			v2f vert(float3 vertex : POSITION) {
				v2f o;
				o.vertex = UnityObjectToClipPos(vertex);
				o.grabPos = ComputeGrabScreenPos(o.vertex);
				o.screenPos = ComputeScreenPos(o.vertex);
				o.posModel = vertex;
				return o;
			}

			static const float DIAMOND_REFR_INDEX = 2.4;
			static const float DIAMOND_CHROMATIC_DISPERSION = 0.16; // um^-1 dn/dlambda

			// Outwards-pointing normal for a cube with soft edges.
			float3 cube_normal(float3 p) {
				float3 dir = sign(p);
				float3 vs = pow(p, 20);
				vs = normalize(vs / (vs.x + vs.y + vs.z));
				vs = pow(vs, 2);
				vs = normalize(vs / (vs.x + vs.y + vs.z));
				return  vs * dir;
			}

			float noise2(float2 v) {
				return frac(sin(dot(v.xy, float2(12.9898, 78.233))) * 43758.5453123);
			}

			// Note:
			// Use of return/discard sometimes cause "invalid access of unbound variable"
			// error in unrelated location.
			float4 frag(v2f data) : SV_Target {
				// Ray in model coordinate (including scaling).
				float3 ray_org = mul(unity_WorldToObject, float4(_WorldSpaceCameraPos, 1)).xyz;
				float3 ray_dir = normalize(data.posModel - ray_org);
				float3 ray_dir_original = ray_dir;

				float3 box_center_w = 0;
				float3 box_extent_w = float3(0.5, 0.5, 0.5);

				float3 curr;
				if (!isect_box(ray_org, ray_dir, box_center_w, box_extent_w, curr)) {
					discard;
					return float4(0, 0, 0, 0);
				}

				float3 accum_radiance = 0;  // Consolidate eye-side path radiance.
				float3 accum_transmittance = 1;

				// Entering light refraction.
				{
					float3 trans_dir;
					float3 refl_dir;
					float coeff_t = refract(ray_dir, cube_normal(curr), DIAMOND_REFR_INDEX, trans_dir, refl_dir);
					float coeff_r = 1 - coeff_t;
					if (in_box(curr, box_center_w, box_extent_w * 0.999)) {
						// already inside --> no reflection
					} else {
						// outside
						accum_radiance += coeff_r * DecodeHDR(UNITY_SAMPLE_TEXCUBE(unity_SpecCube0, refl_dir), unity_SpecCube0_HDR);
						accum_transmittance *= coeff_t;
						ray_dir = trans_dir;
					}
				}

				float step = 0.0067;  // sqrt(3) / MAX_NUM_STEP
				float3 prev = curr;
				uint i;
				[loop]
				for (i = 0; i < MAX_NUM_STEP; i++) {
					// Compute cells.
					float3 cell_coord = (curr + 1) * 8 + 0.5;
					float3 cell_ix3 = floor(cell_coord);
					if (all(5 <= cell_ix3) && all(cell_ix3 < 12) && cell_ix3.x < 11) {
						if (all(frac(cell_coord) < 0.8)) {
							float phase = frac(0.3 * _Time.y); // [0, 1]
							float cycle = floor(0.3 * _Time.y);

							// Attack=0, Decay=0.2
							float duration = 0.2;
							float phase_shift = frac( cell_ix3.x * cell_ix3.y * cell_ix3.z * 0.7) * 0.8;
							bool mask = (phase_shift < phase) & (phase < phase_shift + duration);
							
							float strength =
								mask *
								pow(clamp(1 - (phase - phase_shift) / duration, 0, 1), 0.5) *
								abs(cos(cell_ix3.x * cycle) * cos(cell_ix3.y * cycle * 2) + sin(cell_ix3.z * cycle * 7)) *
								2;
							
							accum_radiance += accum_transmittance * float3(0.1 + 0.3 * sin(cell_ix3.z) * pow(phase_shift, 2), 0.2 + cos(cell_ix3.y) * phase * phase_shift, 0.2 + 0.8 * phase_shift) * strength * step;
						} else if (all(frac(cell_coord) < 0.95)) {
							// frame
							accum_transmittance *= pow(float3(0.99, 0.99, 0.99), step * 100);
						}
						if (all(abs(frac(cell_coord).yz - 0.5) < 0.05)) {
							// beam
							accum_radiance += accum_transmittance * float3(1, 0, 0.3) * step *
								(0.5 * 0.5 * sin(-_Time.y * 2 + length(cell_ix3))) * // carrying wave
								5;
						}
						if (all(frac(cell_coord).xyz < 0.05)) {
							// cube dots
							accum_radiance += accum_transmittance * float3(1, 1, 1) * 10 * step;
						}
					}
					// Isolation plate.
					if (abs(curr.x - 0.3) < 0.02) {
						accum_transmittance *= pow(float3(0.5, 0.5, 0.5), step * 100);
					}
					// Fractal Antenna array.
					if (curr.x > 0.31) {
						float t = (curr.x - 0.31) / 0.2;  // [0, 1]
						float2 apos = 0.5 + curr.yz; // [0, 1]^2
						// Fold/mirror in thickness dir.
						t = 1 - (t > 0.5 ? 1 - t : t) * 2;
						
						float t_ix = t * 4;
						float str = 1;
						[unroll]
						for (uint i = 1; i < 4; i++) {
							if (i < t_ix) {
								apos = frac(apos * 3);
								float2 branch_ix = floor(apos * 3);
								float curr_n = noise2(i * branch_ix * floor(_Time.y * 3));
								float next_n = noise2(i * branch_ix * (floor(_Time.y * 3) + 1));
								str *= 0.2 + 0.8 * lerp(curr_n, next_n, frac(_Time.y * 3));
							}
						}

						if (abs(frac(t_ix) - 0.2) < 0.2) {
							if (all(0.15 < apos) & all(apos < 0.85) & !all(abs(apos - 0.5) < 0.3)) {
								accum_radiance += accum_transmittance * float3(0.2, 0.3 + apos.x, 0.6 + apos.y) * 3 * (floor(t_ix) + 1) * step * str;
							}
						}
					}

					prev = curr;
					curr += ray_dir * step;

					if (!in_box(curr, box_center_w, box_extent_w)) {
						// exit surface.
						break;
					}
				}
				// Exiting light refraction.
				{
					float3 surf_normal = -cube_normal(curr); // pointing inwards
					float3 trans_dir;
					float3 refl_dir;
					float coeff_t = refract(ray_dir, surf_normal, 1 / DIAMOND_REFR_INDEX, trans_dir, refl_dir);
					float coeff_r = 1 - coeff_t;

					accum_radiance +=
						accum_transmittance *  // contribution weight to accumulation at the camera
						coeff_r *
						DecodeHDR(UNITY_SAMPLE_TEXCUBE(unity_SpecCube0, refl_dir), unity_SpecCube0_HDR) *
						accum_transmittance; // cheap way to approximate going back the traversed path
					accum_transmittance *= coeff_t;
					ray_dir = trans_dir;
				}

				// Calculate angle deviation from original.
				float cos_dev = dot(normalize(curr - ray_org), ray_dir_original);

				// Very fake, always radial, extending distortion in screen space.
				float distortion = acos(cos_dev) / 0.78;  // 1 = hfov(45deg)

				// Chromatic abberation. Assume this distortion is for green.
				// red(610nm) / green(550nm) / blue(450nm),
				// half_wl = 0.1um
				float abberation = DIAMOND_CHROMATIC_DISPERSION * 0.1;  // 0.1um = approx wavelength diff between (green, blue) & (green, red).

				float2 bgTexCoord = data.grabPos.xy / data.grabPos.w;
				float2 distortedCoordR = clamp((bgTexCoord - 0.5) * (1 + distortion * (1 - abberation)) + 0.5, 0, 1);
				float2 distortedCoordB = clamp((bgTexCoord - 0.5) * (1 + distortion * (1 + abberation)) + 0.5, 0, 1);
				
				float3 bgRed = tex2D(_BackgroundTexture, distortedCoordR).xyz;
				float3 bgBlue = tex2D(_BackgroundTexture, distortedCoordB).xyz;

				float3 bg = float3(bgRed.r, (bgRed.g + bgBlue.g) * 0.5, bgBlue.b);
				return float4(clamp(accum_radiance + accum_transmittance * bg, 0, 10), 1);
			}
			ENDCG
		}
	}
}
